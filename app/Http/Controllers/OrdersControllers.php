<?php

namespace App\Http\Controllers;

use App\Orders;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrdersControllers extends Controller
{
    //

    public function storeOrder(Request $request)
    {

        $id=md5(Carbon::now());
            $store=Orders::insertGetId([
                "id_order" => $id,
                "vendorId" => $request->input('vendorId'),
                "jasaId" => $request->input('jasaId'),
                "latlongPelanggan" => $request->input('latlongPelanggan'),
                "jasaName" => $request->input('jasaName'),
                "jasaHarga" => $request->input('jasaHarga'),
                "jasaTimes" => $request->input('jasaTimes'),
                "jasaImages" => $request->input('jasaImages'),
                "latlongPejasa" => $request->input('latlongPejasa'),
                "pickUpBy" => $request->input('pickUpBy'),
                "orderBy" => $request->input('orderBy'),
                "orderAt" => $request->input('orderAt'),
                "status" => $request->input('status'),
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]);

            if(!$store){
            return response()->json([
                'status' => true,
                'code' => 200,
                'message' => 'data disave',

            ]);
            }else{
            return response()->json([
                'status' => false,
                'code' => 600,
                'message' => 'data gagal disave',

            ]);
            }


    }

    public function getOrder(Request $request)
    {
        $data=Orders::get();
        return response()->json([
            'status' => true,
            'code' => 200,
            'message' => 'data ditemukan',
            'total_data' => $data->count(),
            'data' => $data
        ]);
    }


    public function updateStatus(Request $request)
    {
        $save=Orders::where('id_order',$request->input('order_id'))->update([
            "status" => $request->input('status'),
            "updated_at" => Carbon::now()
        ]);

        if ($save) {
            return response()->json([
                'status' => true,
                'code' => 200,
                'message' => 'data disave',

            ]);
        }

        return response()->json([
            'status' => false,
            'code' => 600,
            'message' => 'data gagal disave',

        ]);


    }
}
